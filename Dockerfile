FROM golang:1.11-alpine as builder

RUN apk add git openssl openssh alpine-sdk ca-certificates
WORKDIR /root/.ssh
RUN ssh-keyscan gitlab.com >> known_hosts

WORKDIR /build/gitlab.com/CodeBridgeOpen/MapLang
RUN git clone https://gitlab.com/CodeBridgeOpen/MapLang.git .
RUN go mod download
RUN go build -o maplang

FROM alpine:latest

WORKDIR /root/
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /build/gitlab.com/CodeBridgeOpen/MapLang/maplang .
EXPOSE 8085

CMD ["./maplang"]
