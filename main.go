package main

import (
    "github.com/gin-gonic/gin"
    . "map-lang-api/map_lang"
    "net/http"
    "strings"
)

func main() {
    r := gin.Default()
    r.POST("/", performMapping)
    _ = r.Run(":8085")
}

type MappingRequest struct {
    Source  interface{}
    Target  interface{}
    Mapping []string
}

func performMapping(c *gin.Context) {
    var j MappingRequest
    if err := c.ShouldBindJSON(&j); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    tokens, errs := Lex(strings.Join(j.Mapping, "\n"))
    if len(errs) > 0 {
        list := make([]string, 0)
        for _, err := range errs {
            list = append(list, err.Error())
        }

        c.JSON(http.StatusBadRequest, gin.H{
            "error":  "Lexical errors",
            "errors": list,
        })
        return
    }

    out, errs := Parse(tokens, j.Source, j.Target)
    if len(errs) > 0 {
        list := make([]string, 0)
        for _, err := range errs {
            list = append(list, err.Error())
        }

        c.JSON(http.StatusBadRequest, gin.H{
            "error":  "Parser errors",
            "errors": list,
        })
        return
    }

    c.JSON(http.StatusOK, gin.H{
        "result": out,
    })
}
