package main

import (
	"fmt"
	. "map-lang-api/map_lang"
	"strings"
	"testing"
)

var testCaseTwo = strings.Join([]string{
	"map->",
	"    target:test <- 1",
	"    target:test <+ \"tacotruck\"",
}, "\n")

var cases = []map[string]interface{}{
	{
		"tokens": 17,
		"case":   testCaseTwo,
		"source": map[string]interface{}{},
		"target": map[string]interface{}{},
		"test": func(v interface{}) bool {
			i := v.(map[string]interface{})
			return i["test"].(string) == "1tacotruck"
		},
	},
}

func TestLexerAndParser(t *testing.T) {
	for _, c := range cases {
		tokens, errs := Lex(c["case"].(string))

		if len(errs) > 0 {
			t.Fail()
		}

		if c["tokens"].(int) != len(tokens) {
			t.Errorf("Expected %d tokens, got %d tokens", c["tokens"].(int), len(tokens))
		}

		out, errs := Parse(tokens, c["source"], c["target"])
		if len(errs) > 0 {
			t.Fail()
		}

		f := c["test"].(func(v interface{}) bool)

		if !f(out) {
			t.Fail()
		}

		fmt.Println(out)
	}
}
