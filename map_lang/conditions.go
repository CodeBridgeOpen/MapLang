package map_lang

import (
	"fmt"
)

type condition struct {
	i        int
	group    int
	outcomes map[int]bool
	state    conditionsFn
	source   interface{}
	target   interface{}
	tokens   []Token

	expects     []TokenEnum
	groupOpen   bool
	value       interface{}
	operator    TokenEnum
	comparewith Token
}

type conditionsFn func(*condition) conditionsFn

func (c *condition) Apply() bool {
	c.groupOpen = false
	c.outcomes = map[int]bool{
		0: false,
	}

	for c.state = apply; c.state != nil; {
		c.state = c.state(c)
	}

	for _, v := range c.outcomes {
		if v != true {
			return false
		}
	}

	return true
}

func (c *condition) acquire() (val Token) {
	if c.i >= len(c.tokens) {
		c.i++
		return Token{Type: Eof}
	}

	val = c.tokens[c.i]
	c.i++

	return val
}

func (c *condition) grab(i int) (val Token) {
	val = c.tokens[c.i+i]
	return
}

func (c *condition) backup() {
	c.i--
}

func (c *condition) peek() (val Token) {
	val = c.acquire()

	c.backup()

	return
}

func (c *condition) compare() bool {
	return GetOperator(c.operator)(c.value, GetValue(c.comparewith), c.comparewith.Type)
}

func apply(c *condition) conditionsFn {
	if c.i > 0 {
		prev := c.grab(-1)

		if prev.Type == IsAValue(prev.Type) && prev.Type != Selector {
			if c.outcomes[c.group] != true {
				c.outcomes[c.group] = c.compare()
			}
		}
	}

	v := c.peek()
	if len(c.expects) > 0 {
		expected := false
		for _, expect := range c.expects {
			if expect == v.Type {
				expected = true
			}
		}

		if !expected {
			panic(fmt.Sprintf("parser error: unexpected Token %s", v))
		}
	}

	switch v.Type {
	case Conditions:
		c.acquire()
	case Eol:
		return eol
	case Tab:
		return tab
	case Selector:
		return selector
	case Whitespace:
		return whitespace
	case IsAnOperator(v.Type):
		return operator
	case IsAValue(v.Type):
		return value
	case Eof:
		return nil
	}

	return apply
}

func eol(c *condition) conditionsFn {
	c.acquire()

	c.expects = []TokenEnum{
		Tab,
		Eof,
		Eol,
	}

	return apply
}

func tab(c *condition) conditionsFn {
	v := c.grab(-1)
	n := c.grab(1)

	if v.Type == Tab && !c.groupOpen {
		panic(fmt.Sprintf("parser error: conditions-> indentation error, group was not yet opened %s", v))
	}

	if v.Type == Eol && c.groupOpen && n.Type != Tab {
		c.groupOpen = false
		c.group = c.group + 1
		c.outcomes[c.group] = false
		return apply
	}

	c.acquire()

	c.expects = []TokenEnum{
		Selector,
		Tab,
	}

	return apply
}

func selector(c *condition) conditionsFn {
	c.expects = []TokenEnum{
		Whitespace,
	}

	c.groupOpen = true

	v := c.acquire()

	//if GetNestedValue(v, c.target, c.source) == nil {
	//    panic(fmt.Sprintf("parser error: conditions-> illegal selector %s", v.Value))
	//}

	c.value = GetNestedValue(v, c.target, c.source)

	return apply
}

func whitespace(c *condition) conditionsFn {
	v := c.grab(-1)

	c.expects = []TokenEnum{}

	switch v.Type {
	case Selector:
		c.expects = Operators
	case IsAnOperator(v.Type):
		c.expects = Values
	}

	c.acquire()
	return apply
}

func operator(c *condition) conditionsFn {
	v := c.acquire()
	c.operator = v.Type

	c.expects = []TokenEnum{
		Whitespace,
	}

	return apply
}

func value(c *condition) conditionsFn {
	v := c.acquire()
	c.comparewith = v

	c.expects = []TokenEnum{
		Eol,
	}

	return apply
}
