package map_lang

import (
    "regexp"
    "strings"
)

type stateFn func(*lexer) stateFn

type lexer struct {
    line   int
    start  int
    pos    int
    input  string
    Tokens chan Token
    state  stateFn
}

func (l *lexer) next() (val string) {
    if l.pos >= len(l.input) {
        l.pos++
        return ""
    }

    val = l.input[l.pos : l.pos+1]

    l.pos++

    return
}

func (l *lexer) backup() {
    l.pos--
}

func (l *lexer) peek() (val string) {
    val = l.next()

    l.backup()

    return
}

func (l *lexer) emit(t TokenEnum) {
    val := l.input[l.start:l.pos]
    tok := Token{val, l.start, l.line, t, Tokens[t]}
    l.Tokens <- tok
    l.start = l.pos
}

func (l *lexer) Tokenize() {
    lines := strings.Split(l.input, "\n")

    for i, s := range lines {
        l.pos = 0
        l.start = 0
        l.line = i

        l.input = s

        for l.state = lexData; l.state != nil; {
            l.state = l.state(l)
        }
    }

    l.line = l.line + 1
    l.emit(Eof)
}

func lexData(l *lexer) stateFn {
    v := l.peek()

    switch {
    case v == "":
        l.emit(Eol)
        return nil

    case whitespaceRegex.MatchString(v):
        return lexWhitespace
    case stringMarkerRegex.MatchString(v):
        return lexString
    case intRegex.MatchString(v):
        return lexNumber
    }

    return lexWord
}

func lex(t TokenEnum, r *regexp.Regexp) func(*lexer) stateFn {
    return func(l *lexer) stateFn {
        matched := r.FindString(l.input[l.pos:])
        l.pos += len(matched)
        l.emit(t)

        return lexData
    }
}

func lexString(l *lexer) stateFn {
    v := l.input[l.pos:]
    matched := stringRegex.FindString(v)
    l.pos += len(matched)
    l.emit(String)
    return lexData
}

func lexNumber(l *lexer) stateFn {
    v := l.input[l.pos:]

    switch {
    case floatRegex.MatchString(v):
        return lex(Float, floatRegex)
    }

    return lex(Int, intRegex)
}
func lexWhitespace(l *lexer) stateFn {
    v := l.input[l.pos:]

    switch {
    case tabRegex.MatchString(v):
        return lex(Tab, tabRegex)
    }

    return lex(Whitespace, whitespaceRegex)
}

func lexWord(l *lexer) stateFn {
    v := l.input[l.pos:]

    switch {
    case selectorRegex.MatchString(v):
        return lex(Selector, selectorRegex)
    case conditionsRegex.MatchString(v):
        return lex(Conditions, conditionsRegex)
    case passRegex.MatchString(v):
        return lex(Pass, passRegex)
    case failRegex.MatchString(v):
        return lex(Fail, failRegex)
    case mapRegex.MatchString(v):
        return lex(Map, mapRegex)
    case containsRegex.MatchString(v):
        return lex(Contains, containsRegex)
    case setterRegex.MatchString(v):
        return lex(Setter, setterRegex)
    case appendRegex.MatchString(v):
        return lex(Append, appendRegex)
    case smallerThanOrEqualRegex.MatchString(v):
        return lex(SmallerThanOrEqual, smallerThanOrEqualRegex)
    case smallerThanRegex.MatchString(v):
        return lex(SmallerThan, smallerThanRegex)
    case greaterThanOrEqualRegex.MatchString(v):
        return lex(GreaterThanOrEqual, greaterThanOrEqualRegex)
    case greaterThanRegex.MatchString(v):
        return lex(GreaterThan, greaterThanRegex)
    case notEqualsRegex.MatchString(v):
        return lex(NotEquals, notEqualsRegex)
    case equalsRegex.MatchString(v):
        return lex(Equals, equalsRegex)
    case boolRegex.MatchString(v):
        return lex(Bool, boolRegex)
    case wordRegex.MatchString(v):
        return lex(Word, wordRegex)
    }

    return lex(Any, anyRegex)
}

func newLexer(input string) lexer {
    return lexer{0, 0, 0, input, make(chan Token), nil}
}
