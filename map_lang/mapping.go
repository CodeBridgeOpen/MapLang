package map_lang

import (
	"fmt"
	"strconv"
	"strings"
)

type mapping struct {
	i      int
	group  int
	state  mappingFn
	source interface{}
	target interface{}
	tokens []Token

	expects []TokenEnum
	field   string
}

type mappingFn func(*mapping) mappingFn

func (m *mapping) Apply() interface{} {
	m.field = ""

	for m.state = applyMapping; m.state != nil; {
		m.state = m.state(m)
	}

	return m.target
}

func (m *mapping) acquire() (val Token) {
	if m.i >= len(m.tokens) {
		m.i++
		return Token{Type: Eof}
	}

	val = m.tokens[m.i]
	m.i++

	return val
}

func (m *mapping) grab(i int) (val Token) {
	val = m.tokens[m.i+i]
	return
}

func (m *mapping) backup() {
	m.i--
}

func (m *mapping) peek() (val Token) {
	val = m.acquire()

	m.backup()

	return
}

func applyMapping(m *mapping) mappingFn {
	if m.i > 0 {
		prev := m.grab(-1)

		set := false
		if prev.Type == IsAValue(prev.Type) && m.field == "" {
			set = true
			_, m.field = GetTarget(prev)
		}

		if prev.Type == IsAValue(prev.Type) && !set {
			setterType := m.grab(-3).Type

			var v interface{}
			if prev.Type == Selector {
				v = GetNestedValue(prev, m.target, m.source)
			} else {
				v = GetValue(prev)
			}

			if val, ok := v.([]interface{}); ok {
				for i, innerVal := range val {
					m.target = AddValue(Value(innerVal), strings.Split(strings.Replace(m.field, "*", strconv.Itoa(i), 1), "."), m.target, setterType == Setter)
				}
            } else {
				m.target = AddValue(Value(v), strings.Split(m.field, "."), m.target, setterType == Setter)
			}

			m.field = ""
		}
	}

	v := m.peek()
	if len(m.expects) > 0 {
		expected := false
		for _, expect := range m.expects {
			if expect == v.Type {
				expected = true
			}
		}

		if !expected {
			panic(fmt.Sprintf("parser error: unexpected Token %s", v))
		}
	}

	switch v.Type {
	case Map:
		m.acquire()
	case Fail:
		m.expects = []TokenEnum{}
		m.acquire()
	case Pass:
		m.expects = []TokenEnum{}
		m.acquire()
	case Eol:
		return mapEol
	case Tab:
		return mapTab
	case Setter:
		return mapSetter
	case Append:
		return mapAppend
	case Selector:
		return mapSelector
	case Whitespace:
		return mapWhitespace
	case IsAValue(v.Type):
		return mapValue
	case Eof:
		return nil
	}

	return applyMapping
}

func mapEol(m *mapping) mappingFn {
	m.acquire()

	m.expects = []TokenEnum{
		Tab,
		Eof,
		Eol,
	}

	return applyMapping
}

func mapTab(m *mapping) mappingFn {
	m.acquire()

	m.expects = []TokenEnum{
		Selector,
		Tab,
		Pass,
		Fail,
	}

	return applyMapping
}

func mapSetter(m *mapping) mappingFn {
	m.acquire()

	m.expects = []TokenEnum{
		Whitespace,
	}

	return applyMapping
}

func mapAppend(m *mapping) mappingFn {
	m.acquire()

	m.expects = []TokenEnum{
		Whitespace,
	}

	return applyMapping
}

func mapSelector(m *mapping) mappingFn {
	m.expects = []TokenEnum{
		Whitespace,
		Eol,
	}

	v := m.acquire()

	t, _ := GetTarget(v)
	switch t {
	case "source":
		if m.field == "" {
			panic(fmt.Sprintf("parser error: map-> illegal selector, cant mutate source data"))
		}
	}

	return applyMapping
}

func mapWhitespace(c *mapping) mappingFn {
	v := c.grab(-1)

	c.expects = []TokenEnum{}

	switch v.Type {
	case Selector:
		c.expects = []TokenEnum{
			Setter,
			Append,
		}
	case Setter:
	case Append:
		c.expects = Values
	}

	c.acquire()
	return applyMapping
}

func mapValue(c *mapping) mappingFn {
	c.acquire()

	c.expects = []TokenEnum{
		Eol,
	}

	return applyMapping
}
