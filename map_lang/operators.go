package map_lang

import "strconv"

var Operators = []TokenEnum{
	NotEquals,
	Equals,
	Contains,
	GreaterThan,
	GreaterThanOrEqual,
	SmallerThan,
	SmallerThanOrEqual,
}

func IsAnOperator(t TokenEnum) (tok TokenEnum) {
	for _, op := range Operators {
		if op == t {
			return t
		}
	}

	return
}

func GetOperator(t TokenEnum) OperatorFn {
	switch t {
	case NotEquals:
		return notEquals
	case Equals:
		return equals
	case GreaterThan:
		return greater
	case GreaterThanOrEqual:
		return greaterOrEqual
	case SmallerThan:
		return smaller
	case SmallerThanOrEqual:
		return smallerOrEqual
	}

	return nil
}

func notEquals(s interface{}, v interface{}, t TokenEnum) bool {
	switch t {
	case String:
		var sval string

		if s == nil {
			sval = ""
		} else {
			sval = s.(string)
		}

		return sval != v.(string)
	case Int:
		var ival float64

		if s == nil {
			ival = 0
		} else {
			ival = s.(float64)
		}

		return int(ival) != v.(int)
	case Float:
		var fval float64

		if s == nil {
			fval = 0
		} else {
			fval = s.(float64)
		}

		return fval != v.(float64)
	case Bool:
		var bval bool

		if res, ok := s.(bool); ok {
			if s == nil {
				bval = false
			} else {
				bval = res
			}

			return bval != v.(bool)
		}

		if res, err := strconv.ParseBool(s.(string)); err == nil {
			if s == nil {
				bval = false
			} else {
				bval = res
			}

			return bval != v.(bool)
		}
	}

	return false
}

func equals(s interface{}, v interface{}, t TokenEnum) bool {

	switch t {
	case String:
		var sval string

		if s == nil {
			sval = ""
		} else {
			sval = s.(string)
		}

		return sval == v.(string)
	case Int:
		var ival float64

		if s == nil {
			ival = 0
		} else {
			ival = s.(float64)
		}

		return int(ival) == v.(int)
	case Float:
		var fval float64

		if s == nil {
			fval = 0
		} else {
			fval = s.(float64)
		}

		return fval == v.(float64)
	case Bool:
		var bval bool

		if res, ok := s.(bool); ok {
			if s == nil {
				bval = false
			} else {
				bval = res
			}

			return bval == v.(bool)
		}

		sval := ""
		if s == nil {
			sval = "false"
		} else {
			sval = s.(string)
		}

		if res, err := strconv.ParseBool(sval); err == nil {
			if s == nil {
				bval = false
			} else {
				bval = res
			}

			return bval == v.(bool)
		}
	}

	return false
}

func smaller(s interface{}, v interface{}, t TokenEnum) bool {
	if s == nil {
		return true
	}

	switch t {
	case String:
		if res, ok := s.(string); ok {
			return res < v.(string)
		}
	case Int:
		if res, ok := s.(float64); ok {
			return int(res) < v.(int)
		}
	case Float:
		if res, ok := s.(float64); ok {
			return res < v.(float64)
		}
	case Bool:
		panic("parser error: Illegal comparison for Bool: smaller")
	}

	return false
}

func greater(s interface{}, v interface{}, t TokenEnum) bool {
	if s == nil {
		return false
	}

	switch t {
	case String:
		if res, ok := s.(string); ok {
			return res > v.(string)
		}
	case Int:
		if res, ok := s.(float64); ok {
			return int(res) > v.(int)
		}
	case Float:
		if res, ok := s.(float64); ok {
			return res > v.(float64)
		}
	case Bool:
		panic("parser error: Illegal comparison for Bool: greater")
	}

	return false
}

func smallerOrEqual(s interface{}, v interface{}, t TokenEnum) bool {
	if s == nil {
		return true
	}

	switch t {
	case String:
		if res, ok := s.(string); ok {
			return res <= v.(string)
		}
	case Int:
		if res, ok := s.(float64); ok {
			return int(res) <= v.(int)
		}
	case Float:
		if res, ok := s.(float64); ok {
			return res <= v.(float64)
		}
	case Bool:
		panic("parser error: Illegal comparison for Bool: smallerOrEqual")
	}

	return false
}

func greaterOrEqual(s interface{}, v interface{}, t TokenEnum) bool {
	if s == nil {
		return false
	}

	switch t {
	case String:
		if res, ok := s.(string); ok {
			return res >= v.(string)
		}
	case Int:
		if res, ok := s.(float64); ok {
			return int(res) >= v.(int)
		}
	case Float:
		if res, ok := s.(float64); ok {
			return res >= v.(float64)
		}
	case Bool:
		panic("parser error: Illegal comparison for Bool: greaterOrEqual")
	}

	return false
}

type OperatorFn func(s interface{}, v interface{}, t TokenEnum) bool
