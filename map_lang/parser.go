package map_lang

import (
    "errors"
)

type parser struct {
    i                   int
    tokens              []Token
    conditions          bool
    conditionsStartLine int
    conditionTokens     []Token
    mapping             bool
    mappingTokens       []Token
    mappingStartLine    int
    pass                bool
    passTokens          []Token
    passStartLine       int
    fail                bool
    failTokens          []Token
    failStartLine       int
}

func (p *parser) parse(source interface{}, target interface{}) (interface{}, []error) {
    p.keywords()

    errs := make([]error, 0)

    err := p.integrity()
    if len(err) > 0 {
        errs = append(errs, err...)
    }

    p.split()

    pass, e := p.applyConditions(source, target)
    if e != nil {
        errs = append(errs, e)
    }

    target, e = p.doMapping(p.mappingTokens, source, target)
    if e != nil {
        errs = append(errs, e)
    }

    if pass {
        target, e = p.doMapping(p.passTokens, source, target)
        if e != nil {
            errs = append(errs, e)
        }
    } else {
        target, e = p.doMapping(p.failTokens, source, target)
        if e != nil {
            errs = append(errs, e)
        }
    }

    return target, errs
}

func (p *parser) split() {
    if p.conditions {
        p.splitConditions()
    }

    p.splitMapping()
}

func (p *parser) doMapping(tokens []Token, s interface{}, t interface{}) (target interface{}, err error) {
    c := mapping{
        tokens: tokens,
        source: s,
        target: t,
    }

    defer func() {
        if r := recover(); r != nil {

            r, ok := r.(string)
            if ok {
                err = errors.New(r)
            } else {
                panic(r)
            }
        }
    }()

    return c.Apply(), nil
}

func (p *parser) applyConditions(s interface{}, t interface{}) (out bool, err error) {
    c := condition{
        tokens: p.conditionTokens,
        source: s,
        target: t,
    }

    defer func() {
        if r := recover(); r != nil {

            r, ok := r.(string)
            if ok {
                err = errors.New(r)
            } else {
                panic(r)
            }
        }
    }()

    return c.Apply(), nil
}

func (p *parser) splitConditions() {
    for _, tok := range p.tokens {
        if tok.Line >= p.conditionsStartLine && tok.Line < p.mappingStartLine {
            p.conditionTokens = append(p.conditionTokens, tok)
        }
    }

}

func (p *parser) splitMapping() {
    if p.conditions {

        maxMappingLine := p.failStartLine
        if p.fail && p.pass {
            if p.failStartLine > p.passStartLine {
                maxMappingLine = p.passStartLine
            }
        }

        if !p.fail && p.pass {
            maxMappingLine = p.passStartLine
        }

        if p.fail && !p.pass {
            maxMappingLine = p.failStartLine
        }

        for _, tok := range p.tokens {
            if tok.Line >= p.mappingStartLine && tok.Line < maxMappingLine {
                p.mappingTokens = append(p.mappingTokens, tok)
            }
        }

        if p.fail && p.pass {
            if p.failStartLine < p.passStartLine {
                for _, tok := range p.tokens {
                    if tok.Line >= p.failStartLine && tok.Line < p.passStartLine {
                        p.failTokens = append(p.failTokens, tok)
                    }
                }

                for _, tok := range p.tokens {
                    if tok.Line >= p.passStartLine {
                        p.passTokens = append(p.passTokens, tok)
                    }
                }
            } else {
                for _, tok := range p.tokens {
                    if tok.Line >= p.passStartLine && tok.Line < p.failStartLine {
                        p.passTokens = append(p.passTokens, tok)
                    }
                }

                for _, tok := range p.tokens {
                    if tok.Line >= p.failStartLine {
                        p.failTokens = append(p.failTokens, tok)
                    }
                }
            }
        }

        if p.fail && !p.pass {
            for _, tok := range p.tokens {
                if tok.Line >= p.failStartLine {
                    p.failTokens = append(p.failTokens, tok)
                }
            }
        }

        if !p.fail && p.pass {
            for _, tok := range p.tokens {
                if tok.Line >= p.passStartLine {
                    p.passTokens = append(p.passTokens, tok)
                }
            }
        }

    } else {
        for _, tok := range p.tokens {
            if tok.Line >= p.mappingStartLine {
                p.mappingTokens = append(p.mappingTokens, tok)
            }
        }
    }
}

func (p *parser) integrity() []error {
    if p.conditions && !p.mapping {
        return []error{errors.New("parser error: when conditions-> are given, a map-> must provided")}
    }

    if p.conditions && p.mapping && (!p.fail && !p.pass) {
        return []error{errors.New("parser error: when conditions-> are given, either a pass-> or a fail-> must be provided")}
    }

    if !p.conditions && p.mapping && (p.fail || p.pass) {
        if p.fail && p.pass {
            return []error{errors.New("parser error: unexpected pass->, fail->, no conditions provided")}
        } else if p.pass {
            return []error{errors.New("parser error: unexpected pass->, no conditions-> provided")}
        } else {
            return []error{errors.New("parser error: unexpected fail->, no conditions-> provided")}
        }
    }

    return p.positions()
}

func (p *parser) positions() []error {
    err := make([]error, 0)
    if p.conditions {
        if p.fail && (p.failStartLine < p.mappingStartLine) {
            err = append(err, errors.New("parser error: unexpected fail->, occurred before map->"))
        }

        if p.pass && (p.passStartLine < p.mappingStartLine) {
            err = append(err, errors.New("parser error: unexpected pass->, occurred before map->"))
        }

        if p.conditionsStartLine > p.mappingStartLine {
            err = append(err, errors.New("parser error: unexpected map->, cant appear before conditions->"))
        }
    }

    return err
}

func (p *parser) keywords() {
    p.conditions, p.conditionsStartLine = p.has(Conditions)
    p.mapping, p.mappingStartLine = p.has(Map)
    p.pass, p.passStartLine = p.has(Pass)
    p.fail, p.failStartLine = p.has(Fail)
}

func (p *parser) has(t TokenEnum) (bool, int) {
    for _, token := range p.tokens {
        if token.Type == t {
            return true, token.Line
        }
    }

    return false, 0
}

func newParser(t []Token) *parser {
    return &parser{
        tokens: t,
    }
}
