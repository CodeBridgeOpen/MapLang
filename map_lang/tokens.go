package map_lang

import (
	"fmt"
	"regexp"
)

type TokenEnum int8

var wordRegex = regexp.MustCompile(`[A-Za-z]+`)
var stringMarkerRegex = regexp.MustCompile(`"`)
var stringRegex = regexp.MustCompile(`(?mi)".*"`)
var whitespaceRegex = regexp.MustCompile(`\s+`)
var tabRegex = regexp.MustCompile(`(?mi) {4}`)
var conditionsRegex = regexp.MustCompile(`(?mi)conditions->`)
var mapRegex = regexp.MustCompile(`(?mi)map->`)
var passRegex = regexp.MustCompile(`(?mi)pass->`)
var failRegex = regexp.MustCompile(`(?mi)fail->`)
var equalsRegex = regexp.MustCompile(`==|is`)
var greaterThanRegex = regexp.MustCompile(`>|greater\sthan`)
var greaterThanOrEqualRegex = regexp.MustCompile(`>=|greater\sthan\sor\sequal\sto`)
var smallerThanRegex = regexp.MustCompile(`<|smaller\sthan`)
var smallerThanOrEqualRegex = regexp.MustCompile(`<=|smaller\sthan\sor\sequal\sto`)
var notEqualsRegex = regexp.MustCompile(`!=|is\snot`)
var containsRegex = regexp.MustCompile(`contains`)
var selectorRegex = regexp.MustCompile(`(?mi)^\w+:[\w|\.|\*|-]+`)
var setterRegex = regexp.MustCompile(`(?miU)<-`)
var appendRegex = regexp.MustCompile(`(?miU)<\+`)
var intRegex = regexp.MustCompile(`(?mi)\d+`)
var floatRegex = regexp.MustCompile(`(?mi)\d+\.\d+`)
var boolRegex = regexp.MustCompile(`(?m)(true|false)`)
var anyRegex = regexp.MustCompile(`(?mi).+`)

const (
	Eol TokenEnum = iota
	Eof

	Tab
	Word
	Whitespace

	Conditions
	Map
	Pass
	Fail

	NotEquals
	Equals
	Contains
	GreaterThan
	GreaterThanOrEqual
	SmallerThan
	SmallerThanOrEqual

	Setter
	Append
	Selector

	String
	Int
	Float
	Empty
	Bool
	Any
)

type Token struct {
	Value    string
	Pos      int
	Line     int
	Type     TokenEnum `json:"-"`
	TypeName string
}

func (tok Token) String() string {
	return fmt.Sprintf("{%d %s '%s' %d}", tok.Line, Tokens[tok.Type], tok.Value, tok.Pos)
}

var Tokens = map[TokenEnum]string{
	Eol:                "EOL",
	Eof:                "EOF",
	Tab:                "TAB",
	Word:               "WORD",
	Whitespace:         "WHITESPACE",
	Conditions:         "CONDITIONS",
	Map:                "MAP",
	Pass:               "PASS",
	Fail:               "FAIL",
	NotEquals:          "NOT_EQUALS",
	Equals:             "EQUALS",
	Contains:           "CONTAINS",
	GreaterThan:        "GREATER_THAN",
	GreaterThanOrEqual: "GREATER_THAN_OR_EQUAL",
	SmallerThan:        "SMALLER_THAN",
	SmallerThanOrEqual: "SMALLER_THAN_OR_EQUAL",
	Setter:             "SETTER",
	Append:             "APPEND",
	Selector:           "SELECTOR",
	String:             "STRING",
	Int:                "INT",
	Float:              "FLOAT",
	Empty:              "EMPTY",
	Bool:               "BOOL",
	Any:                "ANY",
}
