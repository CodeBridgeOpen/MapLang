package map_lang

import (
    "errors"
    "strconv"
    "strings"
)

type Object map[string]interface{}

func Lex(str string) ([]Token, []error) {
    l := newLexer(str)

    go l.Tokenize()

    tokens := make([]Token, 0)
    errs := make([]error, 0)

    for {
        tok := <-l.Tokens

        tokens = append(tokens, tok)
        if tok.Type == Any || tok.Type == Word {
            errs = append(errs, errors.New(tok.String()))
        }

        if tok.Type == Eof {
            break
        }
    }

    return tokens, errs
}

func Parse(t []Token, source interface{}, target interface{}) (interface{}, []error) {
    p := newParser(t)
    return p.parse(source, target)
}

func GetTarget(t Token) (string, string) {
    pair := strings.Split(t.Value, ":")

    switch pair[0] {
    case "source":
    case "target":
    default:
        panic("")
    }

    return pair[0], pair[1]
}

func AddValue(v interface{}, fields []string, target interface{}, overwrite bool) interface{} {

    f := fields[0]

    _, err := strconv.ParseInt(f, 0, 64)
    if err == nil || f == "*" {
        slice, ok := target.([]interface{})
        if !ok {
            slice = []interface{}{}
        }

        return AddValueToSlice(v, fields, slice, overwrite)
    }

    if _, ok := target.(map[string]interface{}); !ok {
        target = make(map[string]interface{})
    }

    return AddValueToMap(v, fields, target.(map[string]interface{}), overwrite)
}

func AddValueToSlice(v interface{}, fields []string, target []interface{}, overwrite bool) []interface{} {
    f := fields[0]

    i, _ := strconv.ParseInt(f, 0, 64)
    if len(fields) == 1 {
        if !overwrite {
            target[i] = AppendValue(target[i], v)
        } else {
            target[i] = v
        }
        return target
    }

    if len(target) < int(i+1) {
        target = append(target, nil)
    }

    if res, ok := AddValue(v, fields[1:], target[i], overwrite).(map[string]interface{}); ok {
        va := make(map[string]interface{})

        for key, val := range res {
            va[key] = val
        }

        target[i] = va
        return target
    }

    if res, ok := AddValue(v, fields[1:], target[i], overwrite).([]interface{}); ok {
        target[i] = res
        return target
    }

    return target
}

func AddValueToMap(v interface{}, fields []string, target map[string]interface{}, overwrite bool) map[string]interface{} {
    f := fields[0]

    if len(fields) == 1 {
        if !overwrite {
            target[f] = AppendValue(target[f], v)
        } else {
            target[f] = v
        }
        return target
    }

    if target[f] == nil {
        target[f] = map[string]interface{}{}
    }

    if res, ok := AddValue(v, fields[1:], target[f], overwrite).(map[string]interface{}); ok {
        var va map[string]interface{}

        if val, ok := target[f].(map[string]interface{}); !ok {
            va = map[string]interface{}{}
        } else {
            va = val
        }

        for key, val := range res {
            va[key] = val
        }
        target[f] = va
        return target
    }

    if res, ok := AddValue(v, fields[1:], target[f], overwrite).([]interface{}); ok {
        target[f] = res
        return target
    }

    return target
}
