package map_lang

import (
	"strconv"
	"strings"
)

var Values = []TokenEnum{
	String,
	Int,
	Float,
	Empty,
	Bool,
	Selector,
}

func IsAValue(t TokenEnum) (tok TokenEnum) {
	for _, op := range Values {
		if op == t {
			return t
		}
	}

	return Empty
}

func GetValue(t Token) interface{} {
	switch t.Type {
	case String:
		return t.Value[1:(len(t.Value) - 1)]
	case Int:
		val, _ := strconv.Atoi(t.Value)
		return val
	case Float:
		val, _ := strconv.ParseFloat(t.Value, 64)
		return val
	case Bool:
		val, _ := strconv.ParseBool(t.Value)
		return val
	}

	return nil
}

func AppendValue(c interface{}, v interface{}) interface{} {
	if val, ok := v.(string); ok {
		if strings.Contains(val, ".") {
			fval, err := strconv.ParseFloat(val, 64)
			if err == nil {
				currentFval, err := strconv.ParseFloat(c.(string), 64)
				if err != nil {
					panic("Incompatible types to append, attempting to add (float) to (interface{})")
				}
				return fval + currentFval
			}
		}

		_, err := strconv.ParseBool(val)
		if err == nil {
			panic("Incompatible types to append, attempting to add (bool) to (interface{})")
		}
	}

	if val, ok := v.(int); ok {
		if cval, ok := c.(int); ok {
			return cval + val
		}

		if sval, ok := c.(string); ok {
			return sval + strconv.Itoa(val)
		}

		panic("Incompatible types to append, attempting to add (int) to (interface{})")
	}

	if val, ok := v.(float64); ok {
		if cval, ok := c.(float64); ok {
			return cval + val
		}

		if sval, ok := c.(string); ok {
			return sval + strconv.Itoa(int(val))
		}

		if ival, ok := c.(int); ok {
			return ival + int(val)
		}

		panic("Incompatible types to append, attempting to add (float64) to (interface{})")
	}

	if ival, ok := c.(int); ok {
		return strconv.Itoa(ival) + v.(string)
	}

	if sval, ok := v.(string); ok {
		return c.(string) + sval
	}

	if slval, ok := c.([]interface{}); ok {
		c = append(slval, v)
		return c
	}

	return c.(string) + v.(string)
}

func Value(v interface{}) interface{} {
	if val, ok := v.(string); ok {
		if strings.Contains(val, ".") {
			fval, err := strconv.ParseFloat(val, 64)
			if err == nil {
				return fval
			}
		}

		ival, err := strconv.Atoi(val)
		if err == nil {
			return ival
		}

		bval, err := strconv.ParseBool(val)
		if err == nil && "T" != val && "t" != val && "F" != val && "f" != val {
			return bval
		}
	}

	return v
}

func GetNestedValue(token Token, t interface{}, s interface{}) interface{} {
	pair := strings.Split(token.Value, ":")

	obj := GetObject(pair[0], t, s)
	fields := strings.Split(pair[1], ".")

	iterator := false
	for _, f := range fields {
		i, err := strconv.ParseInt(f, 0, 64)
		if err == nil {
			obj = obj.([]interface{})[i]
		} else if f == "*" {
			obj = obj.([]interface{})
			iterator = true
		} else {
			if iterator {
				newObj := make([]interface{}, 0)
				for _, v := range obj.([]interface{}) {
					newObj = append(newObj, v.(map[string]interface{})[f])
				}
				obj = newObj
			} else {
				obj = obj.(map[string]interface{})[f]
			}
		}
	}

	return obj
}

func GetObject(s string, target interface{}, source interface{}) interface{} {

	var o interface{}
	switch s {
	case "source":
		o = source
	case "target":
		o = target
	default:
		panic("")
	}

	return o
}
